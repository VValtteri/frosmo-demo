ACC.datalayer = {
    _autoload: [
        "bindDataLayer"
    ],
    bindDataLayer: function () {
        if ($('#datalayerProductDetailPush').length > 0) {
            this.trackProductPage($('#datalayerProductDetailPush'));

        }
        if($('#datalayerProductHomePagePush').length > 0){
            this.trackHomePage($('#datalayerProductHomePagePush'));
        }
    },

    trackProductPage: function (layer$) {
            var categoriesS = layer$.data('productcategories');
            var categories = categoriesS.split(" ");
        var userGroups = layer$.data('usergroups');
        dataLayer.push({
            frosmoProductId: layer$.data('productcode'),
            frosmoProductCategory: categories.toString(),
            frosmoProductData:userGroups,
            frosmoProductDiscountPrice: 0,
            frosmoProductName: layer$.data('productname'),
            frosmoProductPrice: layer$.data('productprice'),
            frosmoProductPromotionLabel: 'string',
            frosmoProductUrl: layer$.data('producturl'),
            frosmoProductImage:'https://' + window.location.hostname + layer$.data('productimage')
        });
    },
    trackHomePage: function (layer$) {
        var userGroups = layer$.data('usergroups');
        dataLayer.push({
            frosmoProductData:userGroups
        });
    }
};
